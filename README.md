# This is a snakemake repo for tassel-5 GBS workflow.  

Tassel-5 is designed to process GBS data. As a result you need to know about
the experimental setup during the GBS process. This includes the enzymes that
were used to make the library so that the genomic pileups can be confirmed as
being read.

The env is a bit cluttered, so clone it with conda with caution.  

## Setup
1.  Execute the setup rule to build the directory sturture.  
```snakemake setup```  
2.  Copy your reference genome fasta into the ```ref``` directory.  
3.  Copy the keyfile into the ```sequences``` directory.
4.  Copy your sequence fastq (or fastq.gz) file into the ```sequences``` directory.  
5.  Modify the ```config.yaml``` file to include:  
    - enzyme used for GBS  
    - reference file name (don't include the path)  
    - filename of key file (don't include the path)  

## Key file
An example Key file follows.  
```
Flowcell	Lane	Barcode	DNASample	LibraryPlate	Row	Col	LibraryPrepID	LibraryPlateID	Enzyme	BarcodeWell	DNA_Plate	SampleDNA_Well	Genus	Species	Pedigree	Population	SeedLot	FullSampleName
C05F2ACXX	5	CTCC	B73	IBM94_lowvol_Ape	A	1	250020986	450013677	ApeKI	A01	IBM94	A01	zea	mays				B73:C05F2ACXX:5:250020986
C05F2ACXX	5	TTCTC	Mo17	IBM94_lowvol_Ape	B	1	250020994	450013677	ApeKI	B01	IBM94	B01	zea	mays				Mo17:C05F2ACXX:5:250020994
C05F2ACXX	5	GCTTA	M0034	IBM94_lowvol_Ape	C	1	250021031	450013677	ApeKI	C01	IBM94	C01	zea	mays				M0034:C05F2ACXX:5:250021031
C05F2ACXX	5	AACGCCT	M0035	IBM94_lowvol_Ape	D	1	250021014	450013677	ApeKI	D01	IBM94	D01	zea	mays				M0035:C05F2ACXX:5:250021014
C05F2ACXX	5	AGGC	M0077	IBM94_lowvol_Ape	E	1	250021034	450013677	ApeKI	E01	IBM94	E01	zea	mays				M0077:C05F2ACXX:5:250021034
C05F2ACXX	5	TCGTT	M0079	IBM94_lowvol_Ape	F	1	250021046	450013677	ApeKI	F01	IBM94	F01	zea	mays				M0079:C05F2ACXX:5:250021046
C05F2ACXX	5	TGGCTA	M0322	IBM94_lowvol_Ape	G	1	250021058	450013677	ApeKI	G01	IBM94	G01	zea	mays				M0322:C05F2ACXX:5:250021058
C05F2ACXX	5	TGCTGGA	M0323	IBM94_lowvol_Ape	H	1	250021070	450013677	ApeKI	H01	IBM94	H01	zea	mays				M0323:C05F2ACXX:5:250021070
C05F2ACXX	5	TGCA	M0001	IBM94_lowvol_Ape	A	2	250020987	450013677	ApeKI	A02	IBM94	A02	zea	mays				M0001:C05F2ACXX:5:250020987
```  

## Sequence file
The corresponding sequence file.  
```
@HWI-ST2229051101219020921
TAGGAACAGCGCTAGGGGAATGCTAAATTGCTAGCGCCGTAGGGGAGAGTGACATTGTGGTATGCGCTGAGATCGGAAGAG
+HWI-ST2229051101219020921
_b[cceeegccggiiidhfhihiifhiiihiiiiiihfge`ggii_dag_bbccbd_]ba^`cccca^ac^bccccaaac^
@HWI-ST2229051101258420601
TACATCAGCGACGAAGTTGCCATACAAGACGTCCCTGGCGGAGCGAAGACCTTCGAGATCTGCGCGAAGTTCTGCTACGGC
+HWI-ST2229051101258420601
bbbeeeeegggggiiiiihiiiiiiiiiiiiiiiiiiiiiiagecbccccccccccccccdcccccaccccddccbbcccc
@HWI-ST2229051101266220761
GTCAACAGCGGAGAGAGCCTTCACGTTGGCATGCGAGACGTGCCAGATGGAGAGGAACTCGAGGTGGATCATCTCTCCTAG
```   
