# snakemake pipeline for tassel GBS anaylsis
from os.path import join
import os, sys
import glob
from datetime import datetime, date, time

configfile: "./config.yaml"
TASSELPATH=config['TASSELPATH']

# GLOBALS
DIRS = ['sequences', 'logs', 'plots', 'database', 'tmp', 'ref']

rule setup:
    output: DIRS
    shell:
        "mkdir -p "+' '.join(DIRS)

rule clean:
    shell:
        '''
        rm -f database/*
        rm -f logs/*
        rm -f tmp/*
        rm -f ref/*bt2
        '''
rule all:
    input:
        stats='SNPQualStats.txt',

rule build_db:
    input:
        key=join(DIRS[0], config['KEYFILE'])
    output: 'database/database.db'
    log: 'logs/create_db.log'
    params:
        mem=config['RAM'],
        enzyme=config['ENZYME'],
        run=config['TASSELPATH'],
        DB=join(DIRS[3], 'database.db')
    threads: config['CPU']
    shell:
        '''
       {params.run}/run_pipeline.pl -Xmx{params.mem} \
            -fork1 -GBSSeqToTagDBPlugin -db {params.DB} -e {params.enzyme} \
            -i sequences -k {input.key} -endPlugin \
            -runfork1 2>&1 | tee {log}
        '''

rule to_fastq:
    input:
        'database/database.db'
    output: join(DIRS[4], 'tagsForAlign.fq.gz')
    log: 'logs/tagExportToFastq.log'
    params:
        mem=config['RAM'],
        run=config['TASSELPATH'],
        minCount=config['MINCOUNT']
    threads: config['CPU']
    shell:
        '''
       {params.run}/run_pipeline.pl -Xmx{params.mem} \
            -fork1 -TagExportToFastqPlugin -db {input} \
            -o {output} -c {params.minCount} -endPlugin \
            -runfork1 2>&1 | tee {log}
        '''

rule build_ref:
    input: join(DIRS[5], config['REF'])
    output: join(DIRS[5],''.join(config['REF'].split('.')[:-1])) + '.1.bt2'
    log: 'logs/bowtieBuild.log'
    params:
        mem=config['RAM'],
        run=config['TASSELPATH'],
        ref=config['REF'],
        out=join(DIRS[5],''.join(config['REF'].split('.')[:-1]))
    threads: config['CPU']
    shell:
        '''
        bowtie2-build {input} {params.out}
        '''

rule aln_tags:
    input: 
        refind=join(DIRS[5],''.join(config['REF'].split('.')[:-1])) + '.1.bt2',
        seq=join(DIRS[4], 'tagsForAlign.fq.gz')
    output: join(DIRS[4], 'tagsForAlignFullvs.sam')
    log: 'logs/bowtieAln.log'
    params:
        ref=join(DIRS[5],''.join(config['REF'].split('.')[:-1]))
    threads: config['CPU']
    shell:
        '''
        bowtie2 -p {threads} --very-sensitive -x {params.ref} \
           -U {input.seq} -S {output} 2>&1 | tee {log}
       '''

rule sam_to_gbs:
    input: join(DIRS[4], 'tagsForAlignFullvs.sam')
    output: 
        db='database/database.db',
        logs='logs/samtodb.log'
    log: 'logs/samtodb.log'
    params:
        mapper=config['MAPPER'],
        aprop=config['APROP'],
        alen=config['ALEN'],
        minmapq=config['MINMAPQ'],
        mem=config['RAM'],
        run=config['TASSELPATH']
    threads: config['CPU']
    shell:
        '''
       {params.run}/run_pipeline.pl -Xmx{params.mem} \
            -fork1 -SAMToGBSdbPlugin -i {input} -db {output.db} \
            -aProp {params.aprop} -aLen {params.alen} \
            -minMAPQ {params.minmapq} -endPlugin -runfork1 \
            2>&1 | tee {log}
        '''

rule discover_snps:
    input: 'logs/samtodb.log'
    output: 
        db='database/database.db',
        logfile='logs/discover_snps.log'
    log: 'logs/discover_snps.log'
    params:
        sC=config['SC'],
        eC=config['EC'],
        mnLCov=config['mnLCov'],
        mnMAF=config['mnMAF'],
        mem=config['RAM'],
        run=config['TASSELPATH']
    threads: config['CPU']
    shell:
        '''
        {params.run}/run_pipeline.pl \
            -fork1 -DiscoverySNPCallerPluginV2 \
            -db {output.db} -sC {params.sC} \
            -eC {params.eC} -mnLCov {params.mnLCov} \
            -mnMAF {params.mnMAF} -deleteOldData true \
            -endPlugin -runfork1 2>&1 | tee {log}
        '''

rule quality_filter:
    input: 'logs/discover_snps.log'
    output: 'SNPQualStats.txt'
    log: 'logs/quality_report.log'
    threads: config['CPU']
    params:
        mem=config['RAM'],
        run=config['TASSELPATH']
    shell:
        '''
        {params.run}/run_pipeline.pl -Xmx{params.mem} \
            -fork1 -SNPQualityProfilerPlugin \
            -db database/database.db -statFile {output} \
            -deleteOldData true -endPlugin -runfork1 \
            2>&1 | tee {log}
        '''
